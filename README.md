# Scripts
To install, cd the directory of cloned repository and execute:
    
    sudo python ./install.py

To uninstall, cd the directory of cloned repository and execute:

    sudo python ./uninstall.py

Dependencies:
- GitCommit & GitSync scripts require 'git' package for usage
- build script requires 'CMake' and 'make' packages for usage
- convert2mp3 script requires 'lame' package for usage
- djvu2pdf script requires 'ddjvu' package for usage
- convert2mp3 & djvu2pdf & rmeach & MakeClean & pack & unpack scripts require 'zsh' shell for usage
